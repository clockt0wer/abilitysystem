// Fill out your copyright notice in the Description page of Project Settings.


#include "AbilityTypes.h"

FGameplayAbilityInfo::FGameplayAbilityInfo()
	:CooldownDuration(0),
	AbilityCost(0),
	CostType(EAbilityCostType::Mana),
	UIMat(nullptr),
	AbilityClass(nullptr)
{

}

FGameplayAbilityInfo::FGameplayAbilityInfo(float InCooldownDuration, float InAbilityCost, EAbilityCostType InCostType, UMaterialInstance* InUIMaterial, TSubclassOf<UGameplayAbilityBase> InAbilityClass)
	:CooldownDuration(InCooldownDuration),
	AbilityCost(InAbilityCost),
	CostType(InCostType),
	UIMat(InUIMaterial),
	AbilityClass(InAbilityClass)
{

}